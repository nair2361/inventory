package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

public class IntegrationServiceRepositoryTest {
    @Mock
    Inventory inventory;

    @InjectMocks
    InventoryRepository inventoryRepository;

    InventoryService inventoryService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addPart() throws Exception {
        inventoryService=new InventoryService(inventoryRepository);
        InhousePart p1=new InhousePart(1,"Motor",20,9,1,99,1);
        InhousePart p2=new InhousePart(2,"Roata",50,20,1,99,1);
        Product prod1=new Product(1,"Product",10,5,1,99, FXCollections.observableList(Collections.singletonList(p1)));
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableList(Collections.singletonList(p1)));
        Mockito.when(inventory.getProducts()).thenReturn(FXCollections.observableList(Collections.singletonList(prod1)));
        inventoryService.addInhousePart(p2.getName(),p2.getPrice(),p2.getInStock(),p2.getMin(),p2.getMax(), p2.getMachineId());
        assert 1 == inventoryService.getAllParts().size();
    }

    @Test
    void lookUpPart() {
        inventoryService=new InventoryService(inventoryRepository);
        InhousePart p2=new InhousePart(2,"Roata",50,20,1,99,1);
        Mockito.when(inventory.lookupPart("Roata")).thenReturn(p2);
        Part inhousePart=inventoryService.lookupPart("Roata");
        assert 2 == inhousePart.getPartId();
    }

}
