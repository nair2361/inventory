package inventory.service;


import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("InventoryServiceTest")
public class InventoryServiceTest {
    InventoryService inventoryService;

    @BeforeAll
    void init() {
        InventoryRepository inventoryRepository = new InventoryRepository();
        this.inventoryService = new InventoryService(inventoryRepository);
    }

    @ParameterizedTest
    @ValueSource(strings = {"3", "1"})
    void addInhousePartTestDenumireIsNotString(String denumire) {
        String err = "";
        boolean wasAdded = false;
        try {
            wasAdded = inventoryService.addInhousePart(denumire, 2, 2, 0, 100, 12127);

        } catch (Exception e) {

            err = e.getMessage();
        }
        assert (!wasAdded);
        assert (!err.isEmpty());
        err = "";
        try {
            wasAdded = inventoryService.addInhousePart(denumire, 2, -3, 0, 100, 12127);

        } catch (Exception e) {
            err = e.getMessage();
        }
        assert (!wasAdded);
        assert (!err.isEmpty());

    }

    @ParameterizedTest
    @ValueSource(strings = {"produs1", "produs2"})
    void addInhousePartTest(String denumire) {
        String err = "";
        boolean wasAdded = false;
        try {
            wasAdded = inventoryService.addInhousePart(denumire, 2, 2, 0, 100, 12127);

        } catch (Exception e) {
            err = e.getMessage();
        }
        assert (wasAdded);
        err = "";
        wasAdded = false;
        try {
            wasAdded = inventoryService.addInhousePart(denumire, 2, -3, 0, 100, 12127);

        } catch (Exception e) {
            err = e.getMessage();
        }
        assert (!wasAdded);
        assert (!err.isEmpty());

    }

    @Test
    void addInhousePartTestForMaxValues() {
        int inStock = Integer.MAX_VALUE;
        String err = "";
        boolean wasAdded = false;
        try {
            wasAdded = inventoryService.addInhousePart("produs1", 2, inStock, 0, 100, 12127);

        } catch (Exception e) {
            err = e.getMessage();
        }
        assert (wasAdded);
        err = "";
        assert (err.isEmpty());


    }

    @Test
    void addInhousePartTestForInvalidMaxValue() {
        int inStock = Integer.MAX_VALUE;
        String err = "";
        boolean wasAdded = false;
        err = "";
        assert (err.isEmpty());
        inStock++;
        try {
            wasAdded = inventoryService.addInhousePart("produs1", 2, inStock, 0, 100, 12127);

        } catch (Exception e) {
            err = e.getMessage();
        }
        assert (!wasAdded);
        assert (!err.isEmpty());

    }

    @Test
    void addInhousePartTestDenumireWithNumbers() {
        int inStock = Integer.MAX_VALUE;
        String err = "";
        boolean wasAdded = false;
        try {
            wasAdded = inventoryService.addInhousePart("ThiSNameIsLong123456789", 2, inStock, 0, 100, 12127);

        } catch (Exception e) {
            err = e.getMessage();
        }

        assert (wasAdded);
        assert (err.isEmpty());

    }

    @Test
    void addInhousePartTestDenumireIsNullFailure() {
        int inStock = Integer.MAX_VALUE;
        String err = "";
        boolean wasAdded = false;
        try {
            wasAdded = inventoryService.addInhousePart(null, 2, inStock, 0, 100, 12127);

        } catch (Exception e) {
            err = e.getMessage();
        }
        assert (!err.isEmpty());
        assert (!wasAdded);
    }


}
