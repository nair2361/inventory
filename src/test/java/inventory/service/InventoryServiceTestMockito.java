package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

class InventoryServiceTestMockito {

    @Mock
    private InventoryRepository repo;

    @InjectMocks
    private InventoryService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllParts() {
        Part p1=new InhousePart(1,"Motor",20,9,1,99,1);
        Mockito.when(repo.getAllParts()).thenReturn(FXCollections.observableList(Collections.singletonList(p1)));
        assert 1 == service.getAllParts().size();
    }

    @Test
    void addPart() throws Exception {
        InhousePart p1=new InhousePart(1,"Motor",20,9,1,99,1);
        InhousePart p2=new InhousePart(2,"Roata",50,20,1,99,1);
        Boolean addResult=true;
        Mockito.when(repo.getAllParts()).thenReturn(FXCollections.observableList(Collections.singletonList(p1)));
        Mockito.doReturn(addResult).when(repo).addPart(p2);
        service.addInhousePart(p2.getName(),p2.getPrice(),p2.getInStock(),p2.getMin(),p2.getMax(), p2.getMachineId());
        assert 1 == service.getAllParts().size();
    }

    @Test
    void deletePart(){
        Part p1=new InhousePart(1,"Motor",20,9,1,99,1);
        Mockito.when(repo.getAllParts()).thenReturn(FXCollections.observableList(Collections.singletonList(p1)));
        Mockito.doNothing().when(repo).deletePart(p1);
        service.deletePart(p1);
        assert 1 == service.getAllParts().size();
    }
}