package inventory.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {
    private Inventory inventory;

    @BeforeEach
    void setUp() {
        Part p1=new InhousePart(1,"Motor",20,9,1,99,1);
        Part p2=new InhousePart(2,"Roata",50,20,1,99,1);
        Part p3=new InhousePart(3,"Rezervor",80,31,1,99,1);
        Part p4=new InhousePart(4,"Bujie",110,42,1,99,1);
        Part p5=new InhousePart(5,"Sasiuri",140,53,1,99,1);
        Part p6=new InhousePart(6,"Frane",170,64,1,99,1);
        Inventory inventory = new Inventory();
        inventory.addPart(p1);
        inventory.addPart(p2);
        inventory.addPart(p3);
        inventory.addPart(p4);
        inventory.addPart(p5);
        inventory.addPart(p6);
        this.inventory=inventory;
    }

    @AfterEach
    void tearDown() {
        this.inventory=null;
    }

    @Test
    void lookupTestPartIsFoundByNameFirstElement() {
        String nameToSearchFor= "Moto";
        Part partExpected, partFound;
        partExpected=new InhousePart(1,"Motor",20,9,1,99,1);
        partFound=inventory.lookupPart(nameToSearchFor);
        assert(partFound.getPartId()==partExpected.getPartId());
    }

    @Test
    void lookupTestPartIsFoundByIdFirstElement() {
        String nameToSearchFor= "1";
        Part partExpected, partFound;
        partExpected=new InhousePart(1,"Motor",20,9,1,99,1);
        partFound=inventory.lookupPart(nameToSearchFor);
        assert(partFound.getPartId()==partExpected.getPartId());
    }

    @Test
    void lookupTestPartIsFoundByNameSecondElement() {
        String nameToSearchFor= "Roa";
        Part partExpected, partFound;
        partExpected=new InhousePart(2,"Roata",50,20,1,99,1);
        partFound=inventory.lookupPart(nameToSearchFor);
        assert(partFound.getPartId()==partExpected.getPartId());
    }

    @Test
    void lookupTestPartIsFoundByIdSecondElement() {
        String nameToSearchFor= "2";
        Part partExpected, partFound;
        partExpected=new InhousePart(2,"Roata",50,20,1,99,1);
        partFound=inventory.lookupPart(nameToSearchFor);
        assert(partFound.getPartId()==partExpected.getPartId());
    }

    @Test
    void lookupTestInventoryIsEmpty() {
        this.inventory=new Inventory();
        String nameToSearchFor= "Moto";
        Part partExpected, partFound;
        partExpected=null;
        partFound=inventory.lookupPart(nameToSearchFor);
        assert(partFound==partExpected);
    }

    @Test
    void lookupTestPartIsNotFound() {
        String nameToSearchFor= "Abc";
        Part partExpected, partFound;
        partExpected=null;
        partFound=inventory.lookupPart(nameToSearchFor);
        assert(partFound==partExpected);
    }
}