package inventory.model;

import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class IntegrationTest {

    private static InventoryService service;

    @BeforeEach
    void setUp() {
        InventoryRepository repo= new InventoryRepository();
        service = new InventoryService(repo);
    }

    @Test
    void addPart() throws Exception {
        InhousePart p2 = new InhousePart(2, "Roata", 50, 20, 1, 99, 1);
        service.addInhousePart(p2.getName(), p2.getPrice(), p2.getInStock(), p2.getMin(), p2.getMax(), p2.getMachineId());
        assert 11 == service.getAllParts().size();
    }

    @AfterAll
    static void after() {
        InhousePart p2 = new InhousePart(2, "Roata", 50, 20, 1, 99, 1);
        service.deletePart(p2);
    }

    @Test
    void lookUpPart() {
        Part inhousePart = service.lookupPart("Spring");
        assert 2 == inhousePart.getPartId();
    }
}
