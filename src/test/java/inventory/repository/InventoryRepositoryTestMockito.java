package inventory.repository;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

class InventoryRepositoryTestMockito {

    @Mock
    private Inventory inventory;

    @InjectMocks
    private InventoryRepository repo;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addPart() {
        Part p1=new InhousePart(1,"Motor",20,9,1,99,1);
        Part p2=new InhousePart(2,"Roata",50,20,1,99,1);
        Product prod1=new Product(1,"Product",10,5,1,99, FXCollections.observableList(Collections.singletonList(p1)));
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableList(Collections.singletonList(p1)));
        Mockito.when(inventory.getProducts()).thenReturn(FXCollections.observableList(Collections.singletonList(prod1)));
        repo.addPart(p2);
        assert 1 == repo.getAllParts().size();
    }

    @Test
    void getAllParts() {
        Part p1=new InhousePart(1,"Motor",20,9,1,99,1);
        Mockito.when(repo.getAllParts()).thenReturn(FXCollections.observableList(Collections.singletonList(p1)));
        assert 1 == repo.getAllParts().size();
    }

    @Test
    void deletePart() {
        Part p1=new InhousePart(1,"Motor",20,9,1,99,1);
        Product prod1=new Product(1,"Product",10,5,1,99, FXCollections.observableList(Collections.singletonList(p1)));
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableList(Collections.singletonList(p1)));
        Mockito.when(inventory.getProducts()).thenReturn(FXCollections.observableList(Collections.singletonList(prod1)));
        Mockito.doNothing().when(inventory).deletePart(p1);
        repo.deletePart(p1);
        assert 1 == repo.getAllParts().size();
    }

}